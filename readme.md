## To develop this version of babylonjs: ##


```
#!csh
git clone https://github.com/BabylonJS/Babylon.js.git
cd Tools/Gulp
npm install

```

* gulp typescript-all     … to build everything
* gulp webserver          … to start the server.  go to http://localhost:1338/localDev/index.html
* gulp run 		… to watch and rebuild and restart the server

Open the following file in a browser when the server is running:

http://localhost:1338/localDev/index.html

## To clone a copy of the babylonjs library (only), for usage: ##

```
#!csh

mkdir babylonjs
cd babylonjs
git init
git remote add origin https://lfung@bitbucket.org/lfung/babylon.js.git
git config core.sparseCheckout true
echo "dist/dataux release" >> .git/info/sparse-checkout
git pull --depth=1 origin master

cp -R dist/dataux\ release/babylonjs <destination>/lib/babylonjs
```

In your javascript code, include this library using something like this:


```
#!javascript

import BABYLON from '../babylonjs/babylon';

```

## For more information: ##

Refer to BabylonJS developer documentation:
[http://doc.babylonjs.com/generals/how_to_start](Link URL)